'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', (table) => {
      table.uuid("id").primary();
      table.string("email", 254).nullable().default(null).unique();
      table.string("password", 60).nullable().default(null);
      table.string('nama', 100).nullable().notNullable();
      table.string('profesi', 100).nullable().default(null);
      table.string('instansi', 100).nullable().default(null);
      table.text('domisili').nullable().default(null);
      table.string('ktp', 25).nullable().default(null);
      table.string('hp', 100).nullable().default(null);
      table.string('bidang', 100).nullable().default(null);
      table
        .enu("type", ["RELAWAN", "STAFF"])
        .defaultTo("RELAWAN")
        .notNullable();
      table.timestamps();
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
